const createStore = require('redux').createStore;
const combineReducers = require('redux').combineReducers;
const R = require('ramda');

// Combine reducers and create te store
function job(state = {}, action) {
  switch (action.type) {
    case "ADD_JOB":
      return R.uniqBy((job) => JSON.stringify(job))(state.concat(action.job))
    default:
      return state;
  }
}

let store = createStore(
  combineReducers({
    Job: job
  }),
  {
    Job: []
  }
);

module.exports = store;
