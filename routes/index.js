const express = require('express');
const router = express.Router();
const R = require('ramda');
const store = require('../store/AppStore.js');

const responseHandler = (code,message,data) => { return {info: (code==200 ? "Success" : "Failure"), message: message, data: data}  }

const validatePostJob = (req,res,next) => {
  let {name,job_des,min_exp,lang,desig} = req.body;
  if(!name || !job_des || !desig || job_des.length < 5 || !min_exp || isNaN(min_exp) || !lang || lang.length==0 )
    res.status(400).send(responseHandler(400,'Invalid Params ',null))
  else
    next()
}

const validateSearchJob = (req,res,next) => {
  let {lang,exp} = req.query;
  if(!lang || isNaN(exp))
   res.status(400).send(responseHandler(400,'Invalid Params ',null))
  else
   next()
}

router.get('/allJobs', (req,res) => {
  let result = store.getState()["Job"]
  res.status(200).send(responseHandler(200,'All Jobs Fetched Successfully',result))
});

router.post('/job', validatePostJob, (req,res) => {
  let {name,job_des,min_exp,lang,desig} = req.body;
  min_exp = parseInt(min_exp);
  let result = store.dispatch({type: "ADD_JOB",job: {name,job_des,min_exp,lang,desig} });
  console.log("res is ",result);
  res.status(200).send({message: "Job posted"})
})

router.get('/job',validateSearchJob, (req,res) => {
  let {lang,exp} = req.query;
  lang = lang.split(',');
  exp = parseInt(exp);
  let availableJobs = store.getState()["Job"];
  let result = [];
  availableJobs.forEach(job => {
    if(exp >= job.min_exp && R.any(language => job.lang.includes(language))(lang))
      result.push(job);
  })
  if(result.length > 0)
    res.status(200).send(responseHandler(200,'Jobs Found',result));
  else
    res.status(200).send(responseHandler(400,'No Job Found',result));
})


module.exports = router;
