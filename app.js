const express = require("express");
const http = require('http');
const app = express();
const router = express.Router();
const bodyParser = require("body-parser");
const port = process.env.PORT || 8011;
const env = process.env.NODE_ENV || "development";
const morgan = require("morgan");
const config = require("./config")[env];
const gracefulExit = require('express-graceful-exit');
const server = http.createServer(app);
const routes = require("./routes");



/*******  MiddleWare *****/
initMiddlewares = (app) => {

  app.use(function(req, res, next) {
    // res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Content-Type","application/json");
    // res.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS");
    next();
  });

  if ("development" === env) {
    app.use(morgan("dev"));
  }

  app.use(bodyParser.urlencoded({
    extended: false
  }));

  app.use(bodyParser.json());
};
/***** Middlewares End *****/


/****** Routes ******/
initRoutes = (app) => {
  app.get("/", (req, res) => {
    res.status(200).send({message: "UP"});
  });
  app.use("/",routes);
};
/*****Routes End *****/


/***** Graceful Shutdown *****/
gracefullShutdown = (app) => {
  app.use(gracefulExit.middleware(app));
  process.on('SIGINT', () => {
    console.log("Server Recieved Stop Signal.. :( ")
    gracefulExit.gracefulExitHandler(app, server, {
      socketio: app.settings.socketio,
      suicideTimeout: 10 * 1000,
      logger: console.log,
      log: true
    })
  });

  process.on('SIGTERM', () => {
    console.log("Server Recieved Stop Signal.. :( ")
    gracefulExit.gracefulExitHandler(app, server, {
      socketio: app.settings.socketio,
      suicideTimeout: 10 * 1000,
      logger: console.log,
      log: true
    })
  });
}
/***** Graceful Exit End *****/


/***** Start Server *****/
startServer = (server,port) => {
    server.listen(port, function() {
      console.log("Rapido Sheets server listening on port " + port + " in " + env + " mode");
    });
}
/***** Start Server End *****/


initMiddlewares(app);
startServer(server,port);
gracefullShutdown(app);
initRoutes(app);
